#include "header.h"

int snmp_walk(cmts peer)
{
    netsnmp_session session, *ss;
    netsnmp_pdu    *pdu, *response;
    netsnmp_variable_list *vars;
    oid             name[MAX_OID_LEN];
    size_t          name_length;
    int             running;
    int             status;
    int             check;
    int             exitval = 0;
	char			log[256], mac[12];
	int				Q = 0;
	int				length;

	int i;

	cablemodem	*cm;

	pthread_t modems;
	int err_modems;

    netsnmp_ds_register_config(ASN_BOOLEAN, "snmpwalk", "includeRequested",
			       NETSNMP_DS_APPLICATION_ID, 
			       NETSNMP_DS_WALK_INCLUDE_REQUESTED);

    netsnmp_ds_register_config(ASN_BOOLEAN, "snmpwalk", "printStatistics",
			       NETSNMP_DS_APPLICATION_ID, 
			       NETSNMP_DS_WALK_PRINT_STATISTICS);

    netsnmp_ds_register_config(ASN_BOOLEAN, "snmpwalk", "dontCheckOrdering",
			       NETSNMP_DS_APPLICATION_ID,
			       NETSNMP_DS_WALK_DONT_CHECK_LEXICOGRAPHIC);

    SOCK_STARTUP;

    /*
     * open an SNMP session 
     */

	snmp_sess_init(&session);
	session.version = SNMP_VERSION_1;
	session.community = peer.comm;
	session.community_len = strlen(session.community);
	session.peername = peer.cmts_ip;
	session.retries = 15;
	session.timeout = 1000000;
    ss = snmp_open(&session);

    if(ss == NULL)
	{
        /*
         * diagnose snmp_open errors with the input netsnmp_session pointer 
         */
        snmp_sess_perror("snmpwalk", &session);
		sprintf(log, "Error de inicio de sesion snmp contra %s", peer.cmts_ip);
		logger(log_sys, log);
        SOCK_CLEANUP;
        exit(1);
    }

    /*
     * get first object to start walk 
     */
    memmove(name, cmts_oids[0].objid, cmts_oids[0].objid_len * sizeof(oid));
    name_length = cmts_oids[0].objid_len;

	for(i=0;i<cmts_oids[0].objid_len;i++)
	{
		printf("%d.", cmts_oids[0].objid[i]);
	}

    running = 1;

    check = !netsnmp_ds_get_boolean(NETSNMP_DS_APPLICATION_ID, NETSNMP_DS_WALK_DONT_CHECK_LEXICOGRAPHIC);

    while (running) {
        /*
         * create PDU for GETNEXT request and add object name to request 
         */
        pdu = snmp_pdu_create(SNMP_MSG_GETNEXT);
        snmp_add_null_var(pdu, name, name_length);

        /*
         * do the request 
         */
        status = snmp_synch_response(ss, pdu, &response);
        if (status == STAT_SUCCESS) {
            if (response->errstat == SNMP_ERR_NOERROR) {
                /*
                 * check resulting variables 
                 */
                for (vars = response->variables; vars;
                     vars = vars->next_variable) {
                    if ((vars->name_length < cmts_oids[0].objid_len)
                        || (memcmp(cmts_oids[0].objid, vars->name, cmts_oids[0].objid_len * sizeof(oid))
                            != 0)) {
                        /*
                         * not part of this subtree 
                         */
                        running = 0;
                        continue;
                    }

					//cm = (cablemodem *) calloc (1, sizeof(cablemodem));
					print_variable(vars->name, vars->name_length, vars);
/*					for(i=0;i<cmts_q-1;i++)
					{
	                sprintf(mac, "%s", parse_val(vars->val, vars->type));
					length = strlen(mac);
					sprintf(cm->cm_mac, "%c%c%c%c%c%c%c%c%c%c%c%c", mac[length-2], mac[length-1], mac[length-4], mac[length-3], mac[length-6], mac[length-5], mac[length-8], mac[length-7], mac[length-10], mac[length-9], mac[length-12], mac[length-11]);
					cm_ip[cm_ip_len-1] = vars->name[vars->name_length-1];
					sprintf(cm->cm_ip, "%s", snmp_get(ss, cm_ip, cm_ip_len));
					us_snr[us_snr_len-1] = vars->name[vars->name_length-1];
					sprintf(cm->us_snr, "%s", snmp_get(ss, us_snr, us_snr_len));
					us_pwr_cmts[us_pwr_cmts_len-1] = vars->name[vars->name_length-1];
					sprintf(cm->us_pwr_cmts, "%s", snmp_get(ss, us_pwr_cmts, us_pwr_cmts_len));
					}*/

					if(strcmp(cm->cm_ip, "0.0.0.0") != 0)
					{
						get_cm_data(cm);
					}

					//printf("MODEM NRO: %d\tMAC: %s", Q++, cm.cm_mac);

                    if ((vars->type != SNMP_ENDOFMIBVIEW) &&
                        (vars->type != SNMP_NOSUCHOBJECT) &&
                        (vars->type != SNMP_NOSUCHINSTANCE)) {
                        /*
                         * not an exception value 
                         */
                        if (check
                            && snmp_oid_compare(name, name_length, vars->name, vars->name_length) >= 0) {
                            sprintf(log, "Error: OID not increasing");
							logger(log_sys, log);
                            running = 0;
                            exitval = 1;
                        }
                        memmove((char *) name, (char *) vars->name,
                                vars->name_length * sizeof(oid));
                        name_length = vars->name_length;
                    } else
                        /*
                         * an exception value, so stop 
                         */
                        running = 0;
                }
            } else {
                /*
                 * error in response, print it 
                 */
                running = 0;
                if (response->errstat == SNMP_ERR_NOSUCHNAME) {
                    logger(log_sys, "End of MIB");
                } else {
					sprintf(log, "Error in packet. Reason: %s", snmp_errstring(response->errstat));
					logger(log_sys, log);
                    exitval = 2;
                }
            }
        } else if (status == STAT_TIMEOUT) {
            sprintf(log, "Timeout: No Response from %s", session.peername);
			logger(log_sys, log);
            //running = 0;
            exitval = 1;
        } else {                /* status == STAT_ERROR */
            snmp_sess_perror("snmpwalk", ss);
            running = 0;
            exitval = 1;
        }
        if (response)
            snmp_free_pdu(response);
    }

	snmp_close(ss);
    SOCK_CLEANUP;
    return exitval;
}

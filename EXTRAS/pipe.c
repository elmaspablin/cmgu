#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static void child(int fd, const char *string)
{
    pid_t pid = fork();
    int len = strlen(string);
    if (pid < 0)
    {
        fprintf(stderr, "%.5d: failed to fork (%d: %s)\n",
                (int)getpid(), errno, strerror(errno));
        exit(1);
    }
    else if (pid > 0)
        return;
    else if (write(fd, string, len) != len)
    {
        fprintf(stderr, "%.5d: failed to write on pipe %d (%d: %s)\n",
                (int)getpid(), fd, errno, strerror(errno));
        exit(1);
    }
    else
        exit(0);
}

int main (int argc, char *argv[])
{
    char inbuf[100]; //creating an array with a max size of 100
    int p[2]; // Pipe descriptor array

    if (argc != 4)
    {
        fprintf(stderr, "Usage: %s str1 str2 str3\n", argv[0]);
        return 1;
    }

    if (pipe(p) == -1)
    {
        fprintf(stderr, "Pipe Failed"); // pipe fail
        return 1;
    }

	int i;
    for(i = 0; i < 3; i++)
        child(p[1], argv[i+1]);

    int nbytes;
    close(p[1]); // close the write end
    while ((nbytes = read(p[0], inbuf, sizeof(inbuf))) > 0)
        printf("%.*s\n", nbytes, inbuf); // print 

    return 0;
}

#include "header.h"

void agregar_oid()
{
	/*
	char *a = "IF-MIB::ifInOctets.3";
	if(snmp_parse_oid(a, root, &rootlen) != NULL)
	{
		for(i=0;i<rootlen;i++)
		{
			printf("%d.", root[i]);
		}
	}
	else
	{
		printf("NULO\n");
	}
	*/
	/* OIDS DEL CMTS 

	cmts_pri = (struct oid_data *) malloc (sizeof(struct oid_data));
	strcpy(cmts_pri->desc, "CM_MAC");
	cmts_pri->objid[0] = 1;
	cmts_pri->objid[1] = 3;
	cmts_pri->objid[2] = 6;
	cmts_pri->objid[3] = 1;
	cmts_pri->objid[4] = 2;
	cmts_pri->objid[5] = 1;
	cmts_pri->objid[6] = 10;
	cmts_pri->objid[7] = 127;
	cmts_pri->objid[8] = 1;
	cmts_pri->objid[9] = 3;
	cmts_pri->objid[10] = 3;
	cmts_pri->objid[11] = 1;
	cmts_pri->objid[12] = 2;
	cmts_pri->objid_len = 13;

	cmts_ult = (struct oid_data *) malloc (sizeof(struct oid_data));
	cmts_pri->sig = cmts_ult;
	strcpy(cmts_ult->desc, "CM_IP");
	cmts_ult->objid[0] = 1;
	cmts_ult->objid[1] = 3;
	cmts_ult->objid[2] = 6;
	cmts_ult->objid[3] = 1;
	cmts_ult->objid[4] = 2;
	cmts_ult->objid[5] = 1;
	cmts_ult->objid[6] = 10;
	cmts_ult->objid[7] = 127;
	cmts_ult->objid[8] = 1;
	cmts_ult->objid[9] = 3;
	cmts_ult->objid[10] = 3;
	cmts_ult->objid[11] = 1;
	cmts_ult->objid[12] = 3;
	cmts_ult->objid_len = 13;
	//strcpy(cmts_ult->oid_str, "SNMPv2-SMI::transmission.127.1.3.3.1.3.1");

	cmts_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cmts_ult = cmts_ult->sig;
	strcpy(cmts_ult->desc, "CM_US_SNR");
	cmts_ult->objid[0] = 1;
	cmts_ult->objid[1] = 3;
	cmts_ult->objid[2] = 6;
	cmts_ult->objid[3] = 1;
	cmts_ult->objid[4] = 2;
	cmts_ult->objid[5] = 1;
	cmts_ult->objid[6] = 10;
	cmts_ult->objid[7] = 127;
	cmts_ult->objid[8] = 1;
	cmts_ult->objid[9] = 3;
	cmts_ult->objid[10] = 3;
	cmts_ult->objid[11] = 1;
	cmts_ult->objid[12] = 13;
	cmts_ult->objid_len = 13;
	//strcpy(cmts_ult->oid_str, "SNMPv2-SMI::transmission.127.1.3.3.1.13.1");

	cmts_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cmts_ult = cmts_ult->sig;
	strcpy(cmts_ult->desc, "US_PWR_CMTS");
	cmts_ult->objid[0] = 1;
	cmts_ult->objid[1] = 3;
	cmts_ult->objid[2] = 6;
	cmts_ult->objid[3] = 1;
	cmts_ult->objid[4] = 2;
	cmts_ult->objid[5] = 1;
	cmts_ult->objid[6] = 10;
	cmts_ult->objid[7] = 127;
	cmts_ult->objid[8] = 1;
	cmts_ult->objid[9] = 3;
	cmts_ult->objid[10] = 3;
	cmts_ult->objid[11] = 1;
	cmts_ult->objid[12] = 6;
	cmts_ult->objid_len = 13;

	cmts_ult->sig = NULL;

	/* OIDS DE LOS CABLEMODEMS 

	cm_pri = (struct oid_data *) malloc (sizeof(struct oid_data));
	strcpy(cm_pri->desc, "DS1_PWR");
	cm_pri->objid[0] = 1;
	cm_pri->objid[1] = 3;
	cm_pri->objid[2] = 6;
	cm_pri->objid[3] = 1;
	cm_pri->objid[4] = 2;
	cm_pri->objid[5] = 1;
	cm_pri->objid[6] = 10;
	cm_pri->objid[7] = 127;
	cm_pri->objid[8] = 1;
	cm_pri->objid[9] = 1;
	cm_pri->objid[10] = 1;
	cm_pri->objid[11] = 1;
	cm_pri->objid[12] = 6;
	cm_pri->objid[13] = 3;
	cm_pri->objid_len = 14;
	//strcpy(cm_pri->oid_str, "SNMPv2-SMI::transmission.127.1.1.1.1.6.3");

	cm_ult = (struct oid_data *) malloc (sizeof(struct oid_data));
	cm_pri->sig = cm_ult;
	strcpy(cm_ult->desc, "DS2_PWR");
	cm_ult->objid[0] = 1;
	cm_ult->objid[1] = 3;
	cm_ult->objid[2] = 6;
	cm_ult->objid[3] = 1;
	cm_ult->objid[4] = 2;
	cm_ult->objid[5] = 1;
	cm_ult->objid[6] = 10;
	cm_ult->objid[7] = 127;
	cm_ult->objid[8] = 1;
	cm_ult->objid[9] = 1;
	cm_ult->objid[10] = 1;
	cm_ult->objid[11] = 1;
	cm_ult->objid[12] = 6;
	cm_ult->objid[13] = 48;
	cm_ult->objid_len = 14;
	//strcpy(cm_ult->oid_str, "SNMPv2-SMI::transmission.127.1.1.1.1.6.48");

	cm_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cm_ult = cm_ult->sig;
	strcpy(cm_ult->desc, "DS3_PWR");
	cm_ult->objid[0] = 1;
	cm_ult->objid[1] = 3;
	cm_ult->objid[2] = 6;
	cm_ult->objid[3] = 1;
	cm_ult->objid[4] = 2;
	cm_ult->objid[5] = 1;
	cm_ult->objid[6] = 10;
	cm_ult->objid[7] = 127;
	cm_ult->objid[8] = 1;
	cm_ult->objid[9] = 1;
	cm_ult->objid[10] = 1;
	cm_ult->objid[11] = 1;
	cm_ult->objid[12] = 6;
	cm_ult->objid[13] = 49;
	cm_ult->objid_len = 14;
	//strcpy(cm_ult->oid_str, "SNMPv2-SMI::transmission.127.1.1.1.1.6.49");

	cm_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cm_ult = cm_ult->sig;
	strcpy(cm_ult->desc, "DS4_PWR");
	cm_ult->objid[0] = 1;
	cm_ult->objid[1] = 3;
	cm_ult->objid[2] = 6;
	cm_ult->objid[3] = 1;
	cm_ult->objid[4] = 2;
	cm_ult->objid[5] = 1;
	cm_ult->objid[6] = 10;
	cm_ult->objid[7] = 127;
	cm_ult->objid[8] = 1;
	cm_ult->objid[9] = 1;
	cm_ult->objid[10] = 1;
	cm_ult->objid[11] = 1;
	cm_ult->objid[12] = 6;
	cm_ult->objid[13] = 50;
	cm_ult->objid_len = 14;
//	strcpy(cm_ult->oid_str, "SNMPv2-SMI::transmission.127.1.1.1.1.6.50");

	cm_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cm_ult = cm_ult->sig;
	strcpy(cm_ult->desc, "DS1_SNR");
	cm_ult->objid[0] = 1;
	cm_ult->objid[1] = 3;
	cm_ult->objid[2] = 6;
	cm_ult->objid[3] = 1;
	cm_ult->objid[4] = 2;
	cm_ult->objid[5] = 1;
	cm_ult->objid[6] = 10;
	cm_ult->objid[7] = 127;
	cm_ult->objid[8] = 1;
	cm_ult->objid[9] = 1;
	cm_ult->objid[10] = 4;
	cm_ult->objid[11] = 1;
	cm_ult->objid[12] = 5;
	cm_ult->objid[13] = 3;
	cm_ult->objid_len = 14;
	//strcpy(cm_ult->oid_str, "SNMPv2-SMI::transmission.127.1.1.4.1.5.3");

	cm_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cm_ult = cm_ult->sig;
	strcpy(cm_ult->desc, "DS2_SNR");
	cm_ult->objid[0] = 1;
	cm_ult->objid[1] = 3;
	cm_ult->objid[2] = 6;
	cm_ult->objid[3] = 1;
	cm_ult->objid[4] = 2;
	cm_ult->objid[5] = 1;
	cm_ult->objid[6] = 10;
	cm_ult->objid[7] = 127;
	cm_ult->objid[8] = 1;
	cm_ult->objid[9] = 1;
	cm_ult->objid[10] = 4;
	cm_ult->objid[11] = 1;
	cm_ult->objid[12] = 5;
	cm_ult->objid[13] = 48;
	cm_ult->objid_len = 14;
	//strcpy(cm_ult->oid_str, "SNMPv2-SMI::transmission.127.1.1.4.1.5.48");

	cm_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cm_ult = cm_ult->sig;
	strcpy(cm_ult->desc, "DS3_SNR");
	cm_ult->objid[0] = 1;
	cm_ult->objid[1] = 3;
	cm_ult->objid[2] = 6;
	cm_ult->objid[3] = 1;
	cm_ult->objid[4] = 2;
	cm_ult->objid[5] = 1;
	cm_ult->objid[6] = 10;
	cm_ult->objid[7] = 127;
	cm_ult->objid[8] = 1;
	cm_ult->objid[9] = 1;
	cm_ult->objid[10] = 4;
	cm_ult->objid[11] = 1;
	cm_ult->objid[12] = 5;
	cm_ult->objid[13] = 49;
	cm_ult->objid_len = 14;
	//strcpy(cm_ult->oid_str, "SNMPv2-SMI::transmission.127.1.1.4.1.5.49");

	cm_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cm_ult = cm_ult->sig;
	strcpy(cm_ult->desc, "DS4_SNR");
	cm_ult->objid[0] = 1;
	cm_ult->objid[1] = 3;
	cm_ult->objid[2] = 6;
	cm_ult->objid[3] = 1;
	cm_ult->objid[4] = 2;
	cm_ult->objid[5] = 1;
	cm_ult->objid[6] = 10;
	cm_ult->objid[7] = 127;
	cm_ult->objid[8] = 1;
	cm_ult->objid[9] = 1;
	cm_ult->objid[10] = 4;
	cm_ult->objid[11] = 1;
	cm_ult->objid[12] = 5;
	cm_ult->objid[13] = 50;
	cm_ult->objid_len = 14;
	//strcpy(cm_ult->oid_str, "SNMPv2-SMI::transmission.127.1.1.4.1.5.50");

	cm_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cm_ult = cm_ult->sig;
	strcpy(cm_ult->desc, "US1_PWR");
	cm_ult->objid[0] = 1;
	cm_ult->objid[1] = 3;
	cm_ult->objid[2] = 6;
	cm_ult->objid[3] = 1;
	cm_ult->objid[4] = 2;
	cm_ult->objid[5] = 1;
	cm_ult->objid[6] = 10;
	cm_ult->objid[7] = 127;
	cm_ult->objid[8] = 1;
	cm_ult->objid[9] = 2;
	cm_ult->objid[10] = 2;
	cm_ult->objid[11] = 1;
	cm_ult->objid[12] = 3;
	cm_ult->objid[13] = 2;
	cm_ult->objid_len = 14;
	//strcpy(cm_ult->oid_str, "SNMPv2-SMI::transmission.127.1.2.2.1.3.2");

	cm_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cm_ult = cm_ult->sig;
	strcpy(cm_ult->desc, "MIC");
	cm_ult->objid[0] = 1;
	cm_ult->objid[1] = 3;
	cm_ult->objid[2] = 6;
	cm_ult->objid[3] = 1;
	cm_ult->objid[4] = 2;
	cm_ult->objid[5] = 1;
	cm_ult->objid[6] = 10;
	cm_ult->objid[7] = 127;
	cm_ult->objid[8] = 1;
	cm_ult->objid[9] = 1;
	cm_ult->objid[10] = 4;
	cm_ult->objid[11] = 1;
	cm_ult->objid[12] = 6;
	cm_ult->objid[13] = 3;
	cm_ult->objid_len = 14;
	//strcpy(cm_ult->oid_str, "SNMPv2-SMI::transmission.127.1.1.4.1.6.3");

	cm_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cm_ult = cm_ult->sig;
	strcpy(cm_ult->desc, "DS1_TF");
	cm_ult->objid[0] = 1;
	cm_ult->objid[1] = 3;
	cm_ult->objid[2] = 6;
	cm_ult->objid[3] = 1;
	cm_ult->objid[4] = 2;
	cm_ult->objid[5] = 1;
	cm_ult->objid[6] = 2;
	cm_ult->objid[7] = 2;
	cm_ult->objid[8] = 1;
	cm_ult->objid[9] = 10;
	cm_ult->objid[10] = 3;
	cm_ult->objid_len = 11;
	//strcpy(cm_ult->oid_str, "IF-MIB::ifInOctets.3");

	cm_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cm_ult = cm_ult->sig;
	strcpy(cm_ult->desc, "DS2_TF");
	cm_ult->objid[0] = 1;
	cm_ult->objid[1] = 3;
	cm_ult->objid[2] = 6;
	cm_ult->objid[3] = 1;
	cm_ult->objid[4] = 2;
	cm_ult->objid[5] = 1;
	cm_ult->objid[6] = 2;
	cm_ult->objid[7] = 2;
	cm_ult->objid[8] = 1;
	cm_ult->objid[9] = 10;
	cm_ult->objid[10] = 48;
	cm_ult->objid_len = 11;
	//strcpy(cm_ult->oid_str, "IF-MIB::ifInOctets.48"); .1.3.6.1.2.1.2.2.1.10

	cm_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cm_ult = cm_ult->sig;
	strcpy(cm_ult->desc, "DS3_TF");
	cm_ult->objid[0] = 1;
	cm_ult->objid[1] = 3;
	cm_ult->objid[2] = 6;
	cm_ult->objid[3] = 1;
	cm_ult->objid[4] = 2;
	cm_ult->objid[5] = 1;
	cm_ult->objid[6] = 2;
	cm_ult->objid[7] = 2;
	cm_ult->objid[8] = 1;
	cm_ult->objid[9] = 10;
	cm_ult->objid[10] = 49;
	cm_ult->objid_len = 11;
	//strcpy(cm_ult->oid_str, "IF-MIB::ifInOctets.49");

	cm_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cm_ult = cm_ult->sig;
	strcpy(cm_ult->desc, "DS4_TF");
	cm_ult->objid[0] = 1;
	cm_ult->objid[1] = 3;
	cm_ult->objid[2] = 6;
	cm_ult->objid[3] = 1;
	cm_ult->objid[4] = 2;
	cm_ult->objid[5] = 1;
	cm_ult->objid[6] = 2;
	cm_ult->objid[7] = 2;
	cm_ult->objid[8] = 1;
	cm_ult->objid[9] = 10;
	cm_ult->objid[10] = 50;
	cm_ult->objid_len = 11;
	//strcpy(cm_ult->oid_str, "IF-MIB::ifInOctets.50");

	cm_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cm_ult = cm_ult->sig;
	strcpy(cm_ult->desc, "US1_TF");
	cm_ult->objid[0] = 1;
	cm_ult->objid[1] = 3;
	cm_ult->objid[2] = 6;
	cm_ult->objid[3] = 1;
	cm_ult->objid[4] = 2;
	cm_ult->objid[5] = 1;
	cm_ult->objid[6] = 2;
	cm_ult->objid[7] = 2;
	cm_ult->objid[8] = 1;
	cm_ult->objid[9] = 16;
	cm_ult->objid[10] = 4;
	cm_ult->objid_len = 11;
	//strcpy(cm_ult->oid_str, "IF-MIB::ifOutOctets.4");

	cm_ult->sig = (struct oid_data *) malloc (sizeof(struct oid_data));
	cm_ult = cm_ult->sig;
	strcpy(cm_ult->desc, "US2_TF");
	cm_ult->objid[0] = 1;
	cm_ult->objid[1] = 3;
	cm_ult->objid[2] = 6;
	cm_ult->objid[3] = 1;
	cm_ult->objid[4] = 2;
	cm_ult->objid[5] = 1;
	cm_ult->objid[6] = 2;
	cm_ult->objid[7] = 2;
	cm_ult->objid[8] = 1;
	cm_ult->objid[9] = 16;
	cm_ult->objid[10] = 80;
	cm_ult->objid_len = 11;
	//strcpy(cm_ult->oid_str, "IF-MIB::ifOutOctets.80");

	cm_ult->sig = NULL;

	/* FIN DE CARGA DE OIDS */
	
}	

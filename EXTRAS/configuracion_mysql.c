#include "header.h"

void configuracion()
{
	int i, j;
	MYSQL *conexion;
	MYSQL_ROW row;
	MYSQL_RES *res;
	char consulta[256];
	oids *auxiliar;

	conexion = mysql_con();
	mysql_query(conexion, "SELECT * FROM cm_data.oids WHERE 1");
	res = mysql_store_result(conexion);

	/* --------------- CARGA DE OIDS --------------- */

	init_snmp("CARGA DE OIDS");

	if(mysql_num_rows(res))
	{
		for(i=0;i < mysql_num_rows(res); i++)
		{
			row = mysql_fetch_row(res);
			auxiliar = (oids *) malloc (sizeof(oids));
			auxiliar->objid_len = MAX_OID_LEN;
			auxiliar->sig = NULL;
			strcpy(auxiliar->desc, row[1]);
			read_objid(row[2], auxiliar->objid, &auxiliar->objid_len);

			if(strcmp(row[3], "CMTS") == 0)
			{
				if(cmts_oid_list.pri == NULL)
				{
					cmts_oid_list.pri = auxiliar;
					cmts_oid_list.ult = auxiliar;
				}
				else
				{
					cmts_oid_list.ult->sig = auxiliar;
					cmts_oid_list.ult = cmts_oid_list.ult->sig;
				}
			}
			else if(strcmp(row[3], "CM") == 0)
			{
				if(cm_oid_list.pri == NULL)
				{
					cm_oid_list.pri = auxiliar;
					cm_oid_list.ult = auxiliar;
				}
				else
				{
					cm_oid_list.ult->sig = auxiliar;
					cm_oid_list.ult = cm_oid_list.ult->sig;
				}
			}
		}
	}

	/* ---------------------------------------------- */

	strcpy(log_modem, "/var/log/cm_data_modem.log");
	strcpy(log_sys, "/var/log/cm_data_sys.log");
	strcpy(rrd_path, "/var/rrd/modems/");

	struct stat st = {0};

	if (stat(rrd_path, &st) == -1)
	{
		mkdir(rrd_path, 0700);
	}
}

#include "header.h"

void configuracion()
{
	int i, j;

	/* --------------- CARGA DE OIDS --------------- */

	init_snmp("CARGA DE OIDS");

	strcpy(cmts[0]->desc, "CM_MAC");
	read_objid("SNMPv2-SMI::transmission.127.1.3.3.1.2", cmts[0]->objid, &cmts[0]->objid_len);

	strcpy(cmts[1]->desc, "CM_IP");
	read_objid("SNMPv2-SMI::transmission.127.1.3.3.1.3", cmts[1]->objid, &cmts[1]->objid_len);

	strcpy(cmts[2]->desc, "CM_US_SNR");
	read_objid("SNMPv2-SMI::transmission.127.1.3.3.1.13", cmts[2]->objid, &cmts[2]->objid_len);

	strcpy(cmts[3]->desc, "US_PWR_CMTS");
	read_objid("SNMPv2-SMI::transmission.127.1.3.3.1.6", cmts[3]->objid, &cmts[3]->objid_len);

	/* ---------------------------------------------- */

	strcpy(cablemodem[0]->desc, "US_PWR_CMTS");
	read_objid("SNMPv2-SMI::transmission.127.1.3.3.1.6", cablemodem[0]->objid, &cablemodem[0]->objid_len);

	strcpy(cablemodem[1]->desc, "US_PWR_CMTS");
	read_objid("SNMPv2-SMI::transmission.127.1.3.3.1.6", cablemodem[1]->objid, &cablemodem[1]->objid_len);

	strcpy(log_modem, "/var/log/cm_data_modem.log");
	strcpy(log_sys, "/var/log/cm_data_sys.log");
	strcpy(rrd_path, "/var/rrd/modems/");

	struct stat st = {0};

	if (stat(rrd_path, &st) == -1)
	{
		mkdir(rrd_path, 0700);
	}
}

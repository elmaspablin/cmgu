#include "header.h"

void crear_mem(int mem_id, cablemodem *mem)
{
	int i;

	/* VARIABLES DE MEMORIA COMPARTIDA */

	key_t key;

	// INICIALIZACION DE LA MEMORIA

	key = ftok(".", 0);

	if(key < 0)
	{
		logger(log_sys, "Error de ftok - No se puede inicializar memoria compartida.");
		fprintf(stderr, "Error de ftok. Fallo al inicializar la memoria compartida.\n");
		exit(1);
	}
	else
	{
		for(i=0;i<procs;i++)
		{
			mem_id[i] = shmget(key, (1000 * sizeof(cablemodem)), 0777 | IPC_CREAT);

			if(mem_id[i] < 0)
			{
				logger(log_sys, "Error de asignacion de memoria compartida.");
				fprintf(stderr, "Error en la obtención de id de memoria.\n");
				exit(1);
			}

			mem[i] = shmat(mem_id[i], NULL, 0);

			if(!mem[i])
			{
				logger(log_sys, "Error de asignacion de memoria compartida.");
				fprintf(stderr, "Error en asignacion de memoria compartida.\n");
				exit(1);
			}
		}
	}

	/* ------- LIBERAR MEMORIA COMPARTIDA ------- */

	for(i=0;i<procs;i++)
	{
		shmdt(mem[i]);
		printf("Bloque de memoria desafectado...\n");
		shmctl(mem_id[i], IPC_RMID, (struct shmid_ds *)NULL);
		printf("Bloque de memoria destruido...\n");
	}

	/* ------------------------------------------ */

}
